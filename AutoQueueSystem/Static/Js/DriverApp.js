﻿function confirmBooking(requestId) {
    $.ajax({
        type: "POST",
        url: "/DriverApp/ConfirmBooking?driverId=" + driverId + "&requestId=" + requestId,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            if(response)
            {
                elem = document.getElementById(requestId);
                elem.getElementsByClassName("button")[0].innerText = "completeRide";
                document.getElementById("waiting").removeChild(elem);
                document.getElementById("OnGoing").append(elem);
                $("waiting").find(".Error").html("");
                setTimeout(function () { completeRide(requestId) }, 300000);
            }
            else
            {
                $("#" + requestId).find(".Error").html("This Ride is already booked Please refresh page to see latest Bookings");

            }
        },
        error: function () { callback(null, cityId, clickSource); }
    });
}

function completeRide(requestId) {
    $.ajax({
        type: "POST",
        url: "/DriverApp/CompleteRide?requestId=" + requestId,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            if (response) {
                elem = document.getElementById(requestId);
                elem.getElementsByClassName("button")[0].remove();
                document.getElementById("OnGoing").removeChild(elem);
                document.getElementById("Completed").append(elem);
                $("waiting").find(".Error").html("");
                setTimeout(function () { completeRide(requestId) }, 300000);
            }
            else {
                $("#" + requestId).find(".Error").html("This Ride is already booked Please refresh page to see latest Bookings");

            }
        },
        error: function () { callback(null, cityId, clickSource); }
    });
}