﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AutoQueueSystem.Controllers
{
    public class CustomerBookingController : Controller
    {
        
        //[Route("api/CustomerBooking/")]
        [HttpPost]
        public ActionResult Index(int custId)
        {
            AutoBookDAL autoBookDal = new AutoBookDAL();
            bool confirm = autoBookDal.BookRide(custId);
            return Json(confirm);

        }
    }
}
