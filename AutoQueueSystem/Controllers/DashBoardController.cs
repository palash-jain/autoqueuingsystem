﻿using DAL;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AutoQueueSystem.Controllers
{
    public class DashBoardController : Controller
    {
        // GET: DashBoard
        public ActionResult Index()
        {
            AutoBookDAL autoBookDal = new AutoBookDAL();
            List<BookingStatus> driverRidesDetails = autoBookDal.GetAllBookingDetails();
            return View("~/Views/DashBoard/BookingDashboard.cshtml",driverRidesDetails);
        }
    }
}