﻿using DAL;
using Entities;
using Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AutoQueueSystem.Controllers
{
    public class DriverAppController : Controller
    {
        // GET: driverapp
        public ActionResult Index(int driverId)
        {
            AutoBookDAL autoBookDal = new AutoBookDAL();
            List<BookingStatus> driverRidesDetails = autoBookDal.GetDriverBookingStatus(driverId);
            ViewBag.DriverId = driverId;

            return View("~/Views/BookingPages/DriverRidesPage.cshtml", driverRidesDetails);
        }

        [HttpPost]
        public ActionResult ConfirmBooking(int driverId, int requestId)
        {
            AutoBookDAL autoBookDal = new AutoBookDAL();
            bool confirmbooking = autoBookDal.ConfirmRide(requestId, driverId);
            return Json(confirmbooking);
        }

        [HttpPost]
        public ActionResult CompleteRide(int requestId)
        {
            AutoBookDAL autoBookDal = new AutoBookDAL();
            bool confirmbooking = autoBookDal.CompleteRide(requestId);
            return Json(confirmbooking);
        }
    }
}